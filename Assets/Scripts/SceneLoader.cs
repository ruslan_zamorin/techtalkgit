using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private string sceneName;
    [SerializeField] private bool loadOnAwake;

    private void Awake()
    {
        if (loadOnAwake)
            Load();
    }

    public void LoadScene() => Load();
    
    private void Load() => SceneManager.LoadSceneAsync(sceneName);
}
