using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoLoaderDefault : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private List<VideoClip> clips;
    [SerializeField] private Text debugField;
    
    private int clipNumber;

    private void Start() => LoadClip(0);

    public void LoadNexClip()
    {
        videoPlayer.Stop();
        
        clipNumber = (clipNumber + 1) % clips.Count;
        
        DebugInfo("Start load clip");
        LoadClip(clipNumber);
    }

    private void LoadClip(int clipNumber)
    {
        var clip = clips[clipNumber];

        videoPlayer.clip = clip;
        
        DebugInfo($"Clip {clip.name} loaded");
        videoPlayer.Play();
    }

    private void DebugInfo(string info)
    {
        debugField.text = info;
        Debug.Log(info);
    }
}
